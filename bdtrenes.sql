-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-02-2020 a las 13:55:28
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdtrenes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conductor`
--

CREATE TABLE `conductor` (
  `DNI` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `Nº carnet` varchar(12) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conductor`
--

INSERT INTO `conductor` (`DNI`, `Nº carnet`) VALUES
('00193019A', '00193019'),
('15624541C', '15624541'),
('44705955G', '44705955'),
('86198181T', '86198181'),
('87209625L', '87209625');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estaciones`
--

CREATE TABLE `estaciones` (
  `Nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `Nº estacion` int(12) NOT NULL,
  `Ciudad` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estaciones`
--

INSERT INTO `estaciones` (`Nombre`, `Nº estacion`, `Ciudad`) VALUES
('Estación de Madrid Atocha', 5001, 'Madrid'),
('Estación de Barcelona Sants', 5002, 'Barcelona'),
('Valencia-Estación del Norte', 5003, 'Valencia'),
('Estación Sevilla-Santa Justa', 5004, 'Sevilla'),
('Estación de Zaragoza-Delicias', 5005, 'Zaragoza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `Salida` date NOT NULL,
  `Llegada` date NOT NULL,
  `ID tren` int(12) NOT NULL,
  `Nombre estacion salida` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre estacion llegada` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`Salida`, `Llegada`, `ID tren`, `Nombre estacion salida`, `Nombre estacion llegada`) VALUES
('2018-03-20', '2018-03-21', 101, 'Estación de Barcelona Sants', 'Estación de Madrid Atocha'),
('2018-03-22', '2018-03-23', 102, 'Estación de Zaragoza-Delicias', 'Estación Sevilla-Santa Justa'),
('2018-02-14', '2018-02-15', 103, 'Valencia-Estación del Norte', 'Estación Sevilla-Santa Justa'),
('2018-02-21', '2018-02-24', 104, 'Estación Sevilla-Santa Justa', 'Estación de Zaragoza-Delicias'),
('2018-03-23', '2018-03-24', 105, 'Estación de Madrid Atocha', 'Estación de Barcelona Sants');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisor`
--

CREATE TABLE `revisor` (
  `DNI` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `revisor`
--

INSERT INTO `revisor` (`DNI`) VALUES
('19461204F'),
('43385636T'),
('53650254V'),
('83934064A'),
('98755128M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

CREATE TABLE `trabajadores` (
  `DNI` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `Edad` int(3) NOT NULL,
  `Tren_asignado` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`DNI`, `Nombre`, `Edad`, `Tren_asignado`) VALUES
('00193019A', 'CARMEN RIBES PRATS', 26, 103),
('15624541C', 'ANDRES BRIZ MAYORDOMO', 23, 104),
('19461204F', 'MARIA NIEVES LAVIN', 21, 104),
('43385636T', 'ESPERANZA CANAL ALBERDI', 46, 104),
('44705955G', 'FERNANDO FRANCISCO PALOMAR', 39, 105),
('53650254V', 'DAVID MOTOS YUSTE', 47, 105),
('83934064A', 'XAVIER PLANAS ALEGRE', 34, 101),
('86198181T', 'ANDRES CARBALLO FERRER', 27, 102),
('87209625L', 'ROSARIO VERGARA FRANCISCO', 33, 105),
('98755128M', 'VICTOR NICOLAU MERIDA', 54, 101);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trenes`
--

CREATE TABLE `trenes` (
  `ID` int(12) NOT NULL,
  `Año fabricacion` int(4) NOT NULL,
  `Capacidad pasajeros` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `trenes`
--

INSERT INTO `trenes` (`ID`, `Año fabricacion`, `Capacidad pasajeros`) VALUES
(101, 2015, 30),
(102, 2013, 35),
(103, 2014, 40),
(104, 2010, 15),
(105, 2012, 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conductor`
--
ALTER TABLE `conductor`
  ADD PRIMARY KEY (`DNI`),
  ADD UNIQUE KEY `Nº carnet` (`Nº carnet`);

--
-- Indices de la tabla `estaciones`
--
ALTER TABLE `estaciones`
  ADD PRIMARY KEY (`Nº estacion`),
  ADD UNIQUE KEY `Nombre` (`Nombre`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`ID tren`,`Nombre estacion salida`,`Nombre estacion llegada`),
  ADD KEY `Nombre estacion` (`Nombre estacion salida`),
  ADD KEY `ID tren` (`ID tren`),
  ADD KEY `Nombre estacion llegada` (`Nombre estacion llegada`);

--
-- Indices de la tabla `revisor`
--
ALTER TABLE `revisor`
  ADD PRIMARY KEY (`DNI`);

--
-- Indices de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD PRIMARY KEY (`DNI`),
  ADD KEY `Tren asignado` (`Tren_asignado`);

--
-- Indices de la tabla `trenes`
--
ALTER TABLE `trenes`
  ADD PRIMARY KEY (`ID`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `conductor`
--
ALTER TABLE `conductor`
  ADD CONSTRAINT `conductor_ibfk_1` FOREIGN KEY (`DNI`) REFERENCES `trabajadores` (`DNI`);

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`ID tren`) REFERENCES `trenes` (`ID`),
  ADD CONSTRAINT `horario_ibfk_2` FOREIGN KEY (`Nombre estacion salida`) REFERENCES `estaciones` (`Nombre`),
  ADD CONSTRAINT `horario_ibfk_3` FOREIGN KEY (`Nombre estacion llegada`) REFERENCES `estaciones` (`Nombre`);

--
-- Filtros para la tabla `revisor`
--
ALTER TABLE `revisor`
  ADD CONSTRAINT `revisor_ibfk_1` FOREIGN KEY (`DNI`) REFERENCES `trabajadores` (`DNI`);

--
-- Filtros para la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD CONSTRAINT `trabajadores_ibfk_1` FOREIGN KEY (`Tren_asignado`) REFERENCES `trenes` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
