import javax.swing.JOptionPane;

import com.mysql.jdbc.DatabaseMetaData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Conexion {
	public static Connection conexion;
    public static Statement sentencia;
    public static boolean login;
    public static DatabaseMetaData metadata;
    public static Statement enviarJava;
    
    
    public static void cierreConexion() {
    	try {
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public static void aperturaConexion() {
    	// Toda la pesca para la conexion
		try{
	        String controlador="com.mysql.jdbc.Driver";
	        Class.forName(controlador);
	    }catch (Exception e){
	    JOptionPane.showMessageDialog(null,"Error al cargar el controlador");
	    }
	    try{
	     String DSN="jdbc:mysql://localhost/bdtrenes";
	     String user="root";
	     String password="password";
	     conexion=DriverManager.getConnection(DSN,user,password);
	    }
	    catch (Exception e){
	    JOptionPane.showMessageDialog(null,"Error al realizar la conexion \n"+e.toString()+"\n \n ------------\n Esta ventana se cerrara....");
	    }
	    try{
	    	sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	        
	        }catch (Exception e){
	        JOptionPane.showMessageDialog(null,"Error al crear el objeto sentencia");
	        }
    }
    
    // SI ALGUNA NO FUNCIONA EN ALGUNA TABLA, ES POR LAS RELACIONES
    
    public static void nuevaFila() {
    	String tabla = Inicio.seleccion;
    	
    try {
    	
    	if (tabla.equals("trenes")) {
    		enviarJava.executeUpdate("INSERT INTO `trenes` (`ID`, `fecha_fabricacion`, `capacidad`) VALUES ("+AddFila.primerTF.getText().toString()+", "+AddFila.segundoTF.getText().toString()+", "+AddFila.tercerTF.getText().toString()+")");
    	} 
    	else if(tabla.equals("conductor")) {
    		enviarJava.executeUpdate("INSERT INTO `conductor` (`DNI`, `N_carnet`) VALUES ('"+AddFila.primerTF.getText().toString()+"', '"+AddFila.segundoTF.getText().toString()+"'),");
    	}
    	else if(tabla.equals("estaciones")) {
    		enviarJava.executeUpdate("INSERT INTO `estaciones` (`Nombre`, `N_estacion`, `Ciudad`) VALUES ('"+AddFila.primerTF.getText().toString()+"', "+AddFila.segundoTF.getText().toString()+", '"+AddFila.tercerTF.getText().toString()+"')");
    	}
    	else if(tabla.equals("trabajadores")) {
    		enviarJava.executeUpdate("INSERT INTO `trabajadores` (`DNI`, `Nombre`, `Edad`, `Tren_asignado`) VALUES ('"+AddFila.primerTF.getText().toString()+"', '"+AddFila.segundoTF.getText().toString()+"', "+AddFila.tercerTF.getText().toString()+", "+AddFila.cuartoTF.getText().toString()+")");
    	}
    	else if(tabla.equals("revisor")) {
    		enviarJava.executeUpdate("INSERT INTO `revisor` (`DNI`) VALUES ('"+AddFila.primerTF.getText().toString()+"'),");
    	}
    	else if(tabla.equals("horario")) {
    		enviarJava.executeUpdate("INSERT INTO `horario` (`Salida`, `Llegada`, `ID tren`, `Nombre estacion salida`, `Nombre estacion llegada`) VALUES ('"+AddFila.primerTF.getText().toString()+"', '"+AddFila.segundoTF.getText().toString()+"', "+AddFila.tercerTF.getText().toString()+", '"+AddFila.cuartoTF.getText().toString()+"', '"+AddFila.quintoTF.getText().toString()+"')");
    	}
    } catch (SQLException e) {
		e.printStackTrace();
	}
    }
    
    public static void editarFila() {
    	String tabla = Inicio.seleccion;
    	
        try {
        	
        	if (tabla.equals("trenes")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`trenes` SET `fecha_fabricacion` = '"+EditFila.segundoTF.getText().toString()+"', `capacidad` = '"+EditFila.tercerTF.getText().toString()+"' WHERE `trenes`.`ID` = "+EditFila.primerTF.getText().toString()+"");
        	} 
        	else if(tabla.equals("conductor")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`conductor` SET `N_carnet` = '"+EditFila.segundoTF.getText().toString()+"' WHERE `conductor`.`DNI` = '"+EditFila.primerTF.getText().toString()+"'");
        	}
        	else if(tabla.equals("estaciones")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`estaciones` SET `Nombre` = '"+EditFila.primerTF.getText().toString()+"', `Ciudad` = '"+EditFila.tercerTF.getText().toString()+"' WHERE `estaciones`.`N_estacion` = "+EditFila.segundoTF.getText().toString()+"");
        	}
        	else if(tabla.equals("trabajadores")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`trabajadores` SET `Nombre` = '"+EditFila.segundoTF.getText().toString()+"', `Edad` = '"+EditFila.tercerTF.getText().toString()+"', `Tren_asignado` = '"+EditFila.cuartoTF.getText().toString()+"' WHERE `trabajadores`.`DNI` = '"+EditFila.primerTF.getText().toString()+"'");
        	}
        	else if(tabla.equals("revisor")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`revisor` SET `DNI` = '"+EditFila.primerTF.getText().toString()+"' WHERE `revisor`.`DNI` = '"+EditFila.primerTF.getText().toString()+"'");
        	}
        	else if(tabla.equals("horario")) {
        		enviarJava.executeUpdate("UPDATE `bdtrenes`.`horario` SET `Salida` = '"+EditFila.primerTF.getText().toString()+"', `Llegada` = '"+EditFila.segundoTF.getText().toString()+"' WHERE `horario`.`ID tren` = "+EditFila.tercerTF.getText().toString()+"");
        	}
        } catch (SQLException e) {
    		e.printStackTrace();
    	}
    }
    
    // ME DAN ERRORES DE PARENT ROW Y FOREIGN KEY
    public static void borrarFila() throws SQLException {
    	String tabla = Inicio.seleccion;
    	
        	if (tabla.equals("trenes")) {
        		enviarJava.executeUpdate("delete from trenes where ID='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 0).toString()+"'");
        	} 
        	else if(tabla.equals("conductor")) {
        		enviarJava.executeUpdate("delete from conductor where DNI='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 0).toString()+"'");
        	}
        	else if(tabla.equals("estaciones")) {
        		enviarJava.executeUpdate("delete from estaciones where N_estacion='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 1).toString()+"'");
        	}
        	else if(tabla.equals("trabajadores")) {
        		enviarJava.executeUpdate("delete from trabajadores where DNI='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 0).toString()+"'");
        	}
        	else if(tabla.equals("revisor")) {
        		enviarJava.executeUpdate("delete from revisor where DNI='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 0).toString()+"'");
        	}
        	else if(tabla.equals("horario")) {
        		enviarJava.executeUpdate("delete from horario where 'ID tren'='"+Inicio.modelo.getValueAt(Inicio.selectedRow, 2).toString()+"'");
        	}
    }
    
    public static int numeroTablas(){
    	//Contar en numero de tablas en la base de datos
    	ResultSet contadorTablas;
    	int numeroTablas = 0;
    	
		try {
			contadorTablas = enviarJava.executeQuery("SELECT COUNT(*) as tablas FROM information_schema.tables WHERE table_schema = 'bdtrenes'");
			contadorTablas.next();
	    	numeroTablas = contadorTablas.getInt("tablas");
	    	contadorTablas.close();
	    	
		} catch (SQLException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}
    	
    	return numeroTablas;
    }
    
    public static int numeroColumnasTablaSeleccionada(String tabla) throws SQLException {
    	String tablaseleccionada = tabla;
    	// Para contar las columnas de la tabla
    	ResultSet contadorColumnas = enviarJava.executeQuery("SELECT count(*) as columnas FROM information_schema.columns WHERE table_name = '"+tablaseleccionada+"'");
    	contadorColumnas.next();
    	int columnas = contadorColumnas.getInt("columnas");
		contadorColumnas.close();
		return columnas;
		
		
    }
    
    public static ArrayList<String> getTablas() throws SQLException {
    	enviarJava = conexion.createStatement();
    	
		// Conseguir el nombre de las tablas
	    metadata = (DatabaseMetaData) conexion.getMetaData();
	    ResultSet conseguirTablas = metadata.getTables(conexion.getCatalog(), null, "%", null);
	    ArrayList<String> arrayTablas = new ArrayList<String>();
	    
	    for(int i=0; i<numeroTablas();i++) {
	    while(conseguirTablas.next()) {
	    	arrayTablas.add(i, conseguirTablas.getString(3));
		}
	    }
		return arrayTablas;
    }
    
    public static void refrescarTabla() throws SQLException{
    	int numColumnas = numeroColumnasTablaSeleccionada(Inicio.seleccion);
    	enviarJava = conexion.createStatement();
		ResultSet columnas = enviarJava.executeQuery("select COLUMN_NAME from information_schema.COLUMNS where TABLE_NAME='"+Inicio.seleccion+"'");
		
		while(columnas.next()) {
			Inicio.modelo.addColumn(columnas.getString("COLUMN_NAME"));
		}
		columnas.close();
		
    	ResultSet filas = enviarJava.executeQuery("SELECT * FROM `"+Inicio.seleccion+"`");
    	Object [] fila = new Object[numColumnas];
    	while(filas.next()) {
			for(int i=0;i<numColumnas;i++) {
				fila[i] = filas.getObject(i+1);
				
			}
			Inicio.modelo.addRow(fila);
		}
		filas.close();
    	
    }
    
 // Codigo login, esto se mete en una tabla de la base de datos bdtrenes del phpmyadmin y coge los valores que hay en una tabla y los compara
    public static void login() {
    	try {
    		aperturaConexion();
    		
    		enviarJava = conexion.createStatement();
			String usuario = "user1";
			String password = "1234";
				
				if(Inicio.usuario.equals(usuario) && Inicio.contra.equals(password)) {
					login = true;
					JOptionPane.showMessageDialog(null,"SE HA CONECTADO CON EXITO");
					
				} else {
					JOptionPane.showMessageDialog(null,"USUARIO Y CONTRASEÑA INVALIDOS");
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
