import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddFila extends JFrame{

	private JPanel contentPane;
	
	private JLabel lblPrimero;
	private JLabel lblSegundo;
	private JLabel lblTercero;
	private JLabel lblCuarto;
	private JLabel lblQuinto;
	
	public static JTextField primerTF;
	public static JTextField segundoTF;
	public static JTextField tercerTF;
	public static JTextField cuartoTF;
	public static JTextField quintoTF;

	public static JButton btnSalir;
	
	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddFila Addframe = new AddFila();
					Addframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddFila() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 400, 280);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		primerTF = new JTextField();
		primerTF.setColumns(10);
		
		lblPrimero = new JLabel(Inicio.modelo.getColumnName(0));
		if (lblPrimero.getText().toString().equals("A")) {
			lblPrimero.setVisible(false);
			primerTF.setVisible(false);
		}
		
		segundoTF = new JTextField();
		segundoTF.setColumns(10);
		
		lblSegundo = new JLabel(Inicio.modelo.getColumnName(1));
		if (lblSegundo.getText().toString().equals("B")) {
			lblSegundo.setVisible(false);
			segundoTF.setVisible(false);
		}
		
		tercerTF = new JTextField();
		tercerTF.setColumns(10);
		
		lblTercero = new JLabel(Inicio.modelo.getColumnName(2));
		if (lblTercero.getText().toString().equals("C")) {
			lblTercero.setVisible(false);
			tercerTF.setVisible(false);
		}
		
		cuartoTF = new JTextField();
		cuartoTF.setColumns(10);
		
		lblCuarto = new JLabel(Inicio.modelo.getColumnName(3));
		if (lblCuarto.getText().toString().equals("D")) {
			lblCuarto.setVisible(false);
			cuartoTF.setVisible(false);
		}
		
		quintoTF = new JTextField();
		quintoTF.setColumns(10);
		
		lblQuinto = new JLabel(Inicio.modelo.getColumnName(4));
		if (lblQuinto.getText().toString().equals("E")) {
			lblQuinto.setVisible(false);
			quintoTF.setVisible(false);
		}
		
		//Boton y listener de añadir
		JButton btnAadir = new JButton("Añadir");
		btnAadir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
				String primero = primerTF.getText().toString();
				String segundo = segundoTF.getText().toString();
				String tercero = tercerTF.getText().toString();
				String cuarto = cuartoTF.getText().toString();
				String quinto = quintoTF.getText().toString();
				
				Inicio.modelo.addRow(new Object[] {primero, segundo, tercero, cuarto, quinto});
				
				Conexion.nuevaFila();
				} catch(Exception NullPointerException){
					System.out.println(""); //Error sin importancia
				}

			}
		});
		// Boton y listener de borrar campos de los textfield
		JButton btnBorrarCampos = new JButton("Borrar campos");
		btnBorrarCampos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				primerTF.setText("");
				segundoTF.setText("");
				tercerTF.setText("");
				cuartoTF.setText("");
				quintoTF.setText("");
			}
		});
		// Boton salir de la ventana, no se puede salir dandole a la X
		btnSalir = new JButton("Salir");
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(24, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblQuinto)
						.addComponent(lblCuarto)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
									.addComponent(lblTercero)
									.addGap(138))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addComponent(tercerTF, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
										.addComponent(primerTF, Alignment.LEADING)
										.addComponent(lblPrimero, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(lblSegundo, Alignment.LEADING)
										.addComponent(segundoTF, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
										.addComponent(cuartoTF, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
										.addComponent(quintoTF, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
									.addGap(42)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnSalir, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAadir, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnBorrarCampos))
							.addGap(22))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnAadir)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnBorrarCampos)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSalir))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblPrimero)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(primerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblSegundo)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(segundoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblTercero)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tercerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblCuarto)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(cuartoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblQuinto)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(quintoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(50, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
