import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditFila extends JFrame {

	private JPanel contentPane;
	
	private JLabel lblPrimero;
	private JLabel lblSegundo;
	private JLabel lblTercero;
	private JLabel lblCuarto;
	private JLabel lblQuinto;
	
	public static JTextField primerTF;
	public static JTextField segundoTF;
	public static JTextField tercerTF;
	public static JTextField cuartoTF;
	public static JTextField quintoTF;
	
	public static JButton btnSalir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditFila Addframe = new EditFila();
					Addframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditFila() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 319, 296);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		primerTF = new JTextField();
		primerTF.setColumns(10);
		
		lblPrimero = new JLabel(Inicio.modelo.getColumnName(0));
		if (lblPrimero.getText().toString().equals("A")) {
			lblPrimero.setVisible(false);
			primerTF.setVisible(false);
		}
		if (lblPrimero.getText().toString().equals("DNI") || lblPrimero.getText().toString().equals("ID") || lblPrimero.getText().toString().equals("Nombre")) {
			primerTF.setEnabled(false);
			primerTF.setText(Inicio.modelo.getValueAt(Inicio.selectedRow, 0).toString());
		}
		
		segundoTF = new JTextField();
		segundoTF.setColumns(10);
		
		lblSegundo = new JLabel(Inicio.modelo.getColumnName(1));
		if (lblSegundo.getText().toString().equals("B")) {
			lblSegundo.setVisible(false);
			segundoTF.setVisible(false);
		}
		if (lblSegundo.getText().toString().equals("N_estacion")) {
			segundoTF.setEnabled(false);
			segundoTF.setText(Inicio.modelo.getValueAt(Inicio.selectedRow, 1).toString());
		}
		
		tercerTF = new JTextField();
		tercerTF.setColumns(10);
		
		lblTercero = new JLabel(Inicio.modelo.getColumnName(2));
		if (lblTercero.getText().toString().equals("C")) {
			lblTercero.setVisible(false);
			tercerTF.setVisible(false);
		}
		if (lblTercero.getText().toString().equals("ID tren")) {
			tercerTF.setEnabled(false);
			tercerTF.setText(Inicio.modelo.getValueAt(Inicio.selectedRow, 2).toString());
		}
		
		cuartoTF = new JTextField();
		cuartoTF.setColumns(10);
		
		lblCuarto = new JLabel(Inicio.modelo.getColumnName(3));
		if (lblCuarto.getText().toString().equals("D")) {
			lblCuarto.setVisible(false);
			cuartoTF.setVisible(false);
		}
		if ((lblCuarto.getText().toString().equals("Nombre estacion salida"))) {
			cuartoTF.setEnabled(false);
			cuartoTF.setText(Inicio.modelo.getValueAt(Inicio.selectedRow, 3).toString());
		}
		
		quintoTF = new JTextField();
		quintoTF.setColumns(10);
		
		lblQuinto = new JLabel(Inicio.modelo.getColumnName(4));
		if (lblQuinto.getText().toString().equals("E")) {
			lblQuinto.setVisible(false);
			quintoTF.setVisible(false);
		}
		if ((lblQuinto.getText().toString().equals("Nombre estacion llegada"))) {
			quintoTF.setEnabled(false);
			quintoTF.setText(Inicio.modelo.getValueAt(Inicio.selectedRow, 4).toString());
		}
		
		// Boton editar y listener
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
				String primero = primerTF.getText().toString();
				String segundo = segundoTF.getText().toString();
				String tercero = tercerTF.getText().toString();
				String cuarto = cuartoTF.getText().toString();
				String quinto = quintoTF.getText().toString();
				
				Inicio.modelo.setValueAt(primero, Inicio.selectedRow, 0);
				Inicio.modelo.setValueAt(segundo, Inicio.selectedRow, 1);
				Inicio.modelo.setValueAt(tercero, Inicio.selectedRow, 2);
				Inicio.modelo.setValueAt(cuarto, Inicio.selectedRow, 3);
				Inicio.modelo.setValueAt(quinto, Inicio.selectedRow, 4);
				
				} catch(Exception NullPointerException){
					System.out.println(""); //Error sin importancia
				}
				
				Conexion.editarFila();
			}
		});
		
		// Boton borrar y listener
		
		JButton btnBorrarCampos = new JButton("Borrar campos");
		btnBorrarCampos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				primerTF.setText("");
				segundoTF.setText("");
				tercerTF.setText("");
				cuartoTF.setText("");
				quintoTF.setText("");
			}
		});
		
		// Boton salir y listener
		
		btnSalir = new JButton("Salir");
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPrimero)
								.addComponent(primerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSegundo)
								.addComponent(segundoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTercero))
							.addPreferredGap(ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnSalir, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnEditar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnBorrarCampos)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(tercerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(181, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblCuarto)
							.addContainerGap(217, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblQuinto)
							.addContainerGap(217, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(quintoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(181, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(cuartoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(181, Short.MAX_VALUE))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(21)
							.addComponent(btnEditar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnBorrarCampos)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSalir))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblPrimero)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(primerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblSegundo)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(segundoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblTercero)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tercerTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblCuarto)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(cuartoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(lblQuinto)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(quintoTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}

}
