import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JTable;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.event.WindowFocusListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

public class Inicio extends JFrame {

	public static JPanel contentPane;
	
	public JTextField TFusuario;
	public JPasswordField PFcontra;
	public static String usuario;
	public static String contra;
	public static DefaultTableModel modelo;
	public static Inicio frameOpciones;
	private JTable table;
	public static int selectedRow;
	public static String seleccion;
	private JTextField buscarTF;
	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
					// Frame de opciones al salir del programa
					frameOpciones = new Inicio();
					frameOpciones.setSize(200, 150);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inicio() {
		JButton btnBuscar = new JButton("Buscar");
		JComboBox tablaSeleccionada = new JComboBox();
		JButton btnAddFila = new JButton("Añadir fila");
		JButton btnEditFila = new JButton("Editar fila");
		JButton btnBorrarFila = new JButton("Borrar fila");
		JButton btnBorrarTodo = new JButton("Borrar todas filas");
		
		
		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				try{
					if (EditFila.btnSalir.isShowing() == false) {
						tablaSeleccionada.setEnabled(true);
						btnAddFila.setEnabled(true);
						btnBorrarTodo.setEnabled(true);
					}
					
					if (AddFila.btnSalir.isShowing() == false) {
						tablaSeleccionada.setEnabled(true);
						btnAddFila.setEnabled(true);
						btnBorrarTodo.setEnabled(true);
					}
					
				} catch(NullPointerException ex) {
					System.out.print(""); //error sin importancia
				}
				
				
			}
			public void windowLostFocus(WindowEvent e) {
			}
		});
		
		// Iniciacion de la tabla y el modelo
		table = new JTable(modelo);
		buscarTF = new JTextField();
		
		JButton btnConectarse = new JButton("Conectarse");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenuItem mntmSobreElPrograma = new JMenuItem("Sobre el programa");
		menuBar.add(mntmSobreElPrograma);
		mntmSobreElPrograma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayuda.main(null);
			}
		});
		
		// Content pane
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(375, 188, 97, 15);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setBounds(375, 233, 122, 15);
		contentPane.add(lblContrasea);
		
		// Spinner tabla seleccionada
		
				tablaSeleccionada.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							seleccion = (String) tablaSeleccionada.getSelectedItem();
							modelo.setColumnCount(0);
							modelo.setRowCount(0);
							Conexion.refrescarTabla();
						} catch (SQLException e1) {
							// TODO Bloque catch generado automáticamente
							e1.printStackTrace();
						}
					}
				});
				tablaSeleccionada.setBounds(375, 137, 158, 24);
				contentPane.add(tablaSeleccionada);
				tablaSeleccionada.setEnabled(false);
		
		// (Los listeners son action listener porque si no, no funcionan)
		// Listener del boton editar producto
				
		// Por defecto el boton editar esta desactivado
		btnEditFila.setEnabled(false);
		btnEditFila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelo = (DefaultTableModel) table.getModel();
				tablaSeleccionada.setEnabled(false);
				btnEditFila.setEnabled(false);
				btnAddFila.setEnabled(false);
				btnBorrarFila.setEnabled(false);
				btnBorrarTodo.setEnabled(false);
				EditFila.main(null);
			}
		});
		
		// Listener del boton añadir producto
		
		
		btnAddFila.setEnabled(false);
		btnAddFila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelo = (DefaultTableModel) table.getModel();
				tablaSeleccionada.setEnabled(false);
				btnEditFila.setEnabled(false);
				btnAddFila.setEnabled(false);
				btnBorrarFila.setEnabled(false);
				btnBorrarTodo.setEnabled(false);

				AddFila.main(null);
			}
		});
		btnAddFila.setBounds(375, 13, 158, 25);
		contentPane.add(btnAddFila);
		btnEditFila.setBounds(375, 39, 158, 25);
		contentPane.add(btnEditFila);
		
		// Listener del boton borrar fila
		
		btnBorrarFila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] rows = table.getSelectedRows();
				   for(int i=0;i<rows.length;i++){
				     modelo.removeRow(rows[i]-i);
				   }
				   
				   if (table != null && table.getModel() != null) {
					   btnEditFila.setEnabled(false);
					   btnBorrarFila.setEnabled(false);
			        }
				   try {
					Conexion.borrarFila();
				} catch (SQLException e1) {
					// TODO Bloque catch generado automáticamente
					e1.printStackTrace();
				}
			}
		});
				
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 547, 431);
		
		// Por defecto el boton borrar esta desactivado
		btnBorrarFila.setEnabled(false);
		btnBorrarFila.setBounds(375, 65, 158, 25);
		contentPane.add(btnBorrarFila);

		// TextField de usuario y contra
		
		TFusuario = new JTextField();
		TFusuario.setBounds(375, 202, 146, 19);
		contentPane.add(TFusuario);
		TFusuario.setColumns(10);
		
		PFcontra = new JPasswordField();
		PFcontra.setBounds(375, 248, 146, 19);
		contentPane.add(PFcontra);
		
		btnBorrarTodo.setEnabled(false);
		btnBorrarTodo.setBounds(375, 91, 158, 25);
		contentPane.add(btnBorrarTodo);
		
		
		// PARA DESCONECTARSE
				JButton btnDesconectarse = new JButton("Desconectarse");
				btnDesconectarse.setEnabled(false);
				btnDesconectarse.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Conexion.cierreConexion();
						btnAddFila.setEnabled(false);
						btnDesconectarse.setEnabled(false);
						btnConectarse.setEnabled(true);
						tablaSeleccionada.setEnabled(false);
						TFusuario.setVisible(true);
						PFcontra.setVisible(true);
						lblUsuario.setVisible(true);
						lblContrasea.setVisible(true);
						btnBorrarTodo.setEnabled(false);
						btnEditFila.setEnabled(false);
						btnBorrarFila.setEnabled(false);
						btnBuscar.setEnabled(false);
						modelo.setColumnCount(0);
						modelo.setRowCount(0);
						
					}
				});
				btnDesconectarse.setBounds(375, 322, 146, 25);
				contentPane.add(btnDesconectarse);
		
		
		
		// CONEXION
				
		
		btnConectarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				usuario = TFusuario.getText().toString();
				contra = PFcontra.getText().toString();
				
				Conexion.login();
				
				if(Conexion.login == true) {
					btnAddFila.setEnabled(true);
					btnDesconectarse.setEnabled(true);
					btnConectarse.setEnabled(false);
					TFusuario.setText("");
					PFcontra.setText("");
					tablaSeleccionada.setEnabled(true);
					TFusuario.setVisible(false);
					PFcontra.setVisible(false);
					lblUsuario.setVisible(false);
					lblContrasea.setVisible(false);
					btnBorrarTodo.setEnabled(true);
					buscarTF.setEnabled(true);
					btnBuscar.setEnabled(true);
					
					
					try {
						modelo = (DefaultTableModel) table.getModel();
						tablaSeleccionada.setModel(new DefaultComboBoxModel(Conexion.getTablas().toArray()));
						seleccion = (String) tablaSeleccionada.getSelectedItem();
						Conexion.refrescarTabla();
						
					} catch (SQLException e1) {
						// TODO Bloque catch generado automáticamente
						e1.printStackTrace();
					}
					
				} else {
					
				}
				
			}
			
		});
		btnConectarse.setBounds(375, 297, 146, 25);
		contentPane.add(btnConectarse);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 351, 313);
		contentPane.add(scrollPane);
		
		// Listener de raton en la tabla
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnEditFila.setEnabled(true);
				btnBorrarFila.setEnabled(true);
				selectedRow = table.getSelectedRow();
				
			}
			
		});
		
		// Poner el modelo a la tabla y se definen las columnas
		
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		
		scrollPane.setViewportView(table);
		
		JLabel lblBuscar = new JLabel("Filtro:");
		lblBuscar.setBounds(12, 343, 70, 15);
		contentPane.add(lblBuscar);
		
		
		buscarTF.setEnabled(false);
		buscarTF.setBounds(89, 341, 146, 19);
		contentPane.add(buscarTF);
		buscarTF.setColumns(10);
		
		btnBuscar.setEnabled(false);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = buscarTF.getText();
				TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modelo);
			    table.setRowSorter(sorter);
		        if (text.length() == 0) {
		          sorter.setRowFilter(null);
		        } else {
		          sorter.setRowFilter(RowFilter.regexFilter(text));
		        }
			}
		});
		btnBuscar.setBounds(247, 338, 117, 25);
		contentPane.add(btnBuscar);
		
		
		
		// Windows listener para preguntar si esta seguro de salir del programa
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Object[] opciones= { "Si","No" };
				int salir = JOptionPane.showOptionDialog(frameOpciones,"¿Quieres salir?", "Salir",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opciones,opciones[0]);
				if(salir == 0) {
					System.exit(0);
				} else {
					
				}
			}
			
		});
		//Listener de borrar toda la tabla en el menu !!POR SEGURIDAD NO FUNCIONA EN LA BASE DE DATOS PARA NO BORRAR TODO!!
		btnBorrarTodo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rowCount = modelo.getRowCount();
				for (int i = rowCount - 1; i >= 0; i--) {
					modelo.removeRow(i);
					table.removeAll();
				}
				
				
			}
		});
		
	}
}