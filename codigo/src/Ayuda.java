import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JTextPane;

public class Ayuda extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ayuda frame = new Ayuda();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ayuda() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JLabel lblTrabajoTema = new JLabel("Trabajo base datos");
		contentPane.add(lblTrabajoTema, BorderLayout.NORTH);
		
		JLabel lblAutorAdrianGeorge = new JLabel("Autor: Adrian George Anghel");
		lblAutorAdrianGeorge.setVerticalAlignment(SwingConstants.BOTTOM);
		contentPane.add(lblAutorAdrianGeorge, BorderLayout.SOUTH);
		
		JTextPane txtpnTest = new JTextPane();
		txtpnTest.setEditable(false);
		txtpnTest.setText("Programa que se conecta a una base de datos en la nube, la fucion de borrar da errores de parent row y foreign key y no se como solucionarlo, algunos campos no se pueden añadir o editar porque hace falta añadir datos de otras tablas ya que estan todas unidas. La ultima funcion del spinner no la llegue a entender del todo a que te referias ha hacer.\n\nUsuario: user1\nContraseña: 1234");
		contentPane.add(txtpnTest, BorderLayout.CENTER);
	}

}
